#ifndef PETERSENMUTEX_H
#define PETERSENMUTEX_H
#include <atomic>
#include <array>
#include <thread>

class PetersenMutex {
private:
    std::array<std::atomic<bool>, 2> want;
    std::atomic<int> victim;
public:
    PetersenMutex();
    void lock(int i);
    void unlock(int i);
};

#endif // PETERSENMUTEX_H
