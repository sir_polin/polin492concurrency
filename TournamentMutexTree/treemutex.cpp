#include "treemutex.h"

TreeMutex::TreeMutex(std::size_t NumTreads) {
    Deph = 0;
    std::size_t size = 1;
    while (size <= NumTreads) {
        size <<= 1;
        Deph++;
    }
    if (size >> 1 == NumTreads) {
        size >>= 1;
        Deph--;
    }

   Tree = std::move(std::vector<PetersenMutex> (size));
}

std::size_t TreeMutex::left(std::size_t current) {
    return ((current + 1) * 2) - 1;
}
std::size_t TreeMutex::right(std::size_t current) {
    return (current + 1) * 2;
}
std::size_t TreeMutex::parent(std::size_t current) {
    return ((current + 1) / 2) - 1;
}

void TreeMutex::lock(std::size_t TreadIndex) {
    std::size_t i = TreadIndex + Tree.size();
    while (i != 0) {
        Tree[parent(i)].lock(1 - i % 2);
        i = parent(i);
    }
}

void TreeMutex::unlock(std::size_t TreadIndex) {
    std::size_t path = TreadIndex + Tree.size() + 1;
    std::size_t i = 1; //deph counter
    std::size_t j = 0; //node counter
    while (i <= Deph) {
        if (((1 << (Deph - i)) & path) == 0) {
            Tree[j].unlock(0);
            j = left(j);
        } else {
            Tree[j].unlock(1);
            j = right(j);
        }
        ++i;
    }
}
