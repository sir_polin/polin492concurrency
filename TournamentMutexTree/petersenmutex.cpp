#include "petersenmutex.h"

PetersenMutex::PetersenMutex() {
    want[0].store(false);
    want[1].store(false);
    victim.store(0);
}
void PetersenMutex::lock(int i) {
    want[i].store(true);
    victim.store(i);
    while(want[1 - i].load() && victim.load() == i) {
        std::this_thread::yield();
    }
}
void PetersenMutex::unlock(int i) {
    want[i].store(false);
}
