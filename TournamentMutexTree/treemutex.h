#ifndef TREEMUTEX_H
#define TREEMUTEX_H
#include <vector>
#include <utility>

#include"petersenmutex.h"
class TreeMutex {
private:
    std::vector<PetersenMutex> Tree;
    std::size_t Deph;

    std::size_t parent (std::size_t current);
    std::size_t left (std::size_t current);
    std::size_t right (std::size_t current);
public:
    TreeMutex(std::size_t NumTreads);
    void lock(std::size_t TreadIndex);
    void unlock(std::size_t TreadIndex);
};

#endif // TREEMUTEX_H
