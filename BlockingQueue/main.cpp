#include <iostream>
#include <cstdlib>
#include <ctime>
#include <thread>
#include <vector>
#include "BlockingQueue.cpp"

using namespace std;

BlockingQueue<int> Q(5);

void produce() {
    srand(time(0));
    for (int i = 0; i < 16; ++i) {
        int val = i;
        Q.enqueue(val);
        std::this_thread::sleep_for(std::chrono::milliseconds(rand() % 100));
    }
}

bool IsPrime(int n) {
    if (n <= 1)
        return false;
    if (n == 2)
        return true;
    if (n % 2 == 0)
        return false;
    for (int j = 3; j * j <= n; j += 2)
         if (n % j == 0)
             return false;
    return true;
}

void execute() {
    for (int i = 0; i < 4; ++i) {
        int res;
        Q.pop(res);
        std::cout << "thread id " << std::this_thread::get_id() << " : " << res << " " << IsPrime(res) << std::endl;
    }
}


int main() {
    std::vector<std::thread>  consumers;
    std::thread producer(produce);
    for (int i = 0; i < 4; ++i) {
        consumers.push_back(std::move(std::thread(execute)));
    }
    for (int i = 0; i < 4; ++i) {
        consumers[i].join();
    }
    producer.join();
    Q.shutdown();
    std::thread con(execute);
    con.join();
    return 0;
}

