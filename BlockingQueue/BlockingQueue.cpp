#include <thread>
#include <condition_variable>
#include <mutex>
#include <deque>
#include <atomic>
#include <exception>
#include <utility>

class QueueShutDownException : public std::exception {
public:
    virtual const char* what() const throw() {
        return "The queue was shutted down";
    }
};

template <class Value, class Container = std::deque<Value>>
class BlockingQueue {
public:
    explicit BlockingQueue (std::size_t capacity): _capacity(capacity), isWorking(true) { }

    void enqueue(Value item) {
        std::unique_lock<std::mutex> lock(QueueMutex);
        if (!isWorking) {
            throw  QueueShutDownException();
            return;
        }
        while (_queue.size() == _capacity) {
            notFull.wait(lock);
        }
        _queue.push_back(std::move(item));
        notEmpty.notify_one();
    }

    void pop(Value& item) {
        std::unique_lock<std::mutex> lock(QueueMutex);
        if (!isWorking && _queue.empty()) {
            throw  QueueShutDownException();
            return;
        }
        while (_queue.empty()) {
            if (!isWorking) {
                throw  QueueShutDownException();
            }
            notEmpty.wait(lock);
        }
        item = std::move(_queue.front());
        _queue.pop_front();
        notFull.notify_one();
    }
    void shutdown() {
        std::unique_lock<std::mutex> lock(QueueMutex);
        isWorking = false;
        notEmpty.notify_all();
        notFull.notify_all();
    }

private:
    Container _queue;
    std::size_t _capacity;
    BlockingQueue(const BlockingQueue&) = delete;
    void operator=(const BlockingQueue&) = delete;
    bool isWorking;
    std::mutex QueueMutex;
    std::condition_variable notEmpty;
    std::condition_variable notFull;
};


