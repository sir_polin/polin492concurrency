#include "BlockingQueue.cpp"
#include <future>
#include <functional>
#include <vector>

const std::size_t DefaultNumWorkers = 4;

template <class Value>
class ThreadPool {
public:
    ThreadPool() {
        SetDefaultNumWorkers();
        StartWorkers();
    }

    ThreadPool(std::size_t NumThreads) : NumWorkers(NumThreads) {
        StartWorkers();
    }

    std::future<Value> submit (std::packaged_task<Value()> task) {
        std::packaged_task<Value()> TempTask = task;
        std::future<Value> TempFuture = TempTask.get_future();
        TaskQueue.enqueue(std::move(TempTask));
        return TempFuture;
    }

    void shutdown () {
        TaskQueue.shutdown();
        for (std::thread& worker : Workers) {
            if (worker.joinable()) {
                worker.join();
            }
        }
    }

    ~ThreadPool() {
        shutdown();
    }

private:
    std::vector<std::thread> Workers;
    std::size_t NumWorkers;
    BlockingQueue<std::packaged_task<Value()>> TaskQueue;

    void SetDefaultNumWorkers() {
        std::size_t TempNum = std::thread::hardware_concurrency();
        NumWorkers = (TempNum < 1) ? DefaultNumWorkers : TempNum;
    }

    void GetNewTask() {
        std::packaged_task<Value()> task;
        while(TaskQueue.pop(task)) {
            task();
        }
    }

    void StartWorkers() {
        for (std::size_t i = 0; i < NumWorkers; ++i) {
            Workers.emplace_back(std::thread([&]() {
                                                    std::packaged_task<Value()> task;
                                                    try {
                                                        while (true) {
                                                            TaskQueue.pop(task);
                                                            task();
                                                        }
                                                    } catch (...) {
                                                        return;
                                                    }
                                                  }));
        }
    }
};
